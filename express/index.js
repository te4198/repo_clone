/* const http = require('http');

const server = http.createServer((req, res) => {
    res.status = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end("hello world");
});

server.listen(3000, () => {
    console.log('server on port 3000');

}); */

/* lo mismo pero con express */

const express = require('express');

const morgan = require('morgan');

const app = express();


//SETTINGS 

app.set('AppName', 'este es el nombre de la aplicacion');       /* configuraciones de expres esta muestra el nombre de la aplicacion */
app.set('port', 3000);                                          /* configuraciones de expres esta cambia el puerto donde funciona la app */                                    

//motores de plantillas
// ejecutar [npm i ejs]

app.set('view engine', 'ejs');





//----------------------------------------------------------------
//EXPRESS MIDDLEWARES

/* function logger(req, res, next) {           //control de usuarios
    console.log(`route received: ${req.protocol}:// ${req.get('host')} ${req.originalUrl}`);        //devuelve la direccion que el usuario pide
    next();
}
 */


//------------------------------




app.use(express.json());
// app.use(logger);
app.use(morgan('dev'));     //logger muestra en consola las peticiones que llegan






/* app.all('/user', (req, res, next) => {        //procesa las entradas a esa ruta para entrar primero ahi[control]
    console.log('por aqui paso');
    next();                                     //redirecciona al siguiente ruta
}); */





//peticiones al servidor probar con POSTMAN

/* app.get('/', (req, res) => {        
    res.send('hello world con express');
});
 */

//plantillas

app.get('/', (req, res) => {
    const  data =[{name: 'test'}, {name: 'test2'}, {name: 'test'}];
    res.render('index.ejs', {people: data});
});


app.get('/metodoget', (req, res) => {           /* la peticione GET sirve para devolver cosas */
    res.send('peticion get');
});

app.post('/metodopost', (req, res) => {         /* la peticione POST sirve para recibir determinado dato, almacenarlo en un BD y realizar una respuesta */
    res.send('peticion post');
});

app.put('/metodoput', (req, res) => {           /* la peticion PUT sirve para tomar los datos que nos da el FRONTEND, actualizarlos y devolver datos */
    res.send('peticion put');
});

app.delete('/metododelete', (req, res) => {     /* la peticion DELETE sirve para cuando el FRONTEND envia una peticion delete y eliminar algo en el servidor y devuelve una respuesta  */
    res.send('delete request recibido');
});





//----------------------------------------------------------------
//ENRUTAMIENTO
// instalar nodemon para reiniciar el servidor en automatico [npm i nodemon -D]
//para ejecutar el servidor en tiempo real escribir [npx nodemon index.js]

app.get('/user', (req, res) => {           
    res.json({
        name: 'cameron',
        lastname: 'home'
    });
});

app.post('/user/:id', (req, res) => {
    console.log(req.body);                  //cuerpo de la peticion
    console.log(req.params);                //parametro de la peticion
    res.send('peticion post');
});

app.put('/user/:id', (req, res) => {           
    console.log(req.body);
    res.send(`usuario ${req.params.id} actualizado`);
});


app.delete('/user/:userID', (req, res) => {     
    res.send(`usuario ${req.params.userID} deleted`);       //usar tildes invertidas para concatenar las respuestas
});


app.use(express.static('public'));          //carpetas estaticas de carga como HTML, CSS, JS 

app.listen(app.get('port'), () => {
    console.log(app.get('AppName'));        //quiero obetener el nombre de la aplicacion
    console.log('server on port', app.get('port'));
});

//------------------------------
//------------------------------
//STATIC FILES

