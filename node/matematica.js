/* Archivo donde estan las funciones de matematica */

console.log("hello world!");
console.log("------------------------");

const Math ={};     /* creas un objeto */


function sumar(x1, x2) {
    return x1 + x2;
}

function restar(x1, x2) {
    return x1 - x2;
}

function multiplicar(x1, x2) {
    return x1 * x2;
}

function dividir(x1, x2) {
    if (x2 == 0) {
        console.log("no se puede dividir por cero");
    } else{
        return x1 / x2;
    }

}


// exports.sumar = sumar;              /* estas exportando los resultados de las funciones al index */
// exports.restar = restar;            /* estas exportando los resultados de las funciones al index */
// exports.multiplicar = multiplicar;  /* estas exportando los resultados de las funciones al index */
// exports.dividir = dividir;          /* estas exportando los resultados de las funciones al index */

Math.sumar = sumar;                     /* se usa el objeto para poder exportar las funciones */
Math.restar = restar;                   /* se usa el objeto para poder exportar las funciones */
Math.multiplicar = multiplicar;         /* se usa el objeto para poder exportar las funciones */
Math.dividir = dividir;                 /* se usa el objeto para poder exportar las funciones */

module.exports = Math;                  /* se exporta el objeto */

function hello(name) {
    console.log("Hello", name);
}

//module.exports = hello;