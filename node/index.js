const math = require('./matematica.js'); /* como importar archivos dentro de otros */


console.log(math.sumar(1,2));           /* llama a las funciones desde otro archivo */             
console.log(math.restar(1,2));          /* llama a las funciones desde otro archivo */     
console.log(math.multiplicar(1,2));     /* llama a las funciones desde otro archivo */         
console.log(math.dividir(1,2));         /* llama a las funciones desde otro archivo */     

console.log(math.sumar(4,2));
console.log(math.restar(6,2));
console.log(math.multiplicar(7,2));
console.log(math.dividir(4,2));

console.log("----------------------------------------------------------------")

console.log(math);          /* muestra lo que se esta almacenando el modulo */

//como llamar modulos propios de node.js
//-------------------------------

const os =require('os');

console.log(os.platform());
console.log(os.release());
console.log(os.freemem());
console.log(os.totalmem());

//interactuar con los archivos del sistema desde codigo node.js
//llamar al modulo
const fs = require('fs');
// crear archivos nuevos
// fs. metodo
// writeFile = crear archivos nuevos
// ./texto.txt = primer parametro el nombre y ubicacion del nuevo archivo
// 'linea 1' = segundo parametro el contenido del archivo
// function(){---} = ejecucion de una funcion para ver si ocurrio algun error


// fs.writeFile('./texto.txt', 'linea 1', function(err){       /* funcion con contenido de codigo asincrono */
//     if (err) {                                              /* ejecucion de codigo mientras aun se esta ejecutando la funcion */
//         console.log(err);                                   /* realiza operaciones que no son su responsabilidad */
//     }                                                       /* delega las tareas, en este caso al sistema operativo */
//     console.log("archivo creado");
// });

// console.log("ultima linea de codigo");                      /* la ejecucion esta realizando por delegacion primero ejecuta node.js y luego el SO */                  

// fs.readFile('./texto.txt', function(err, data){                 /* leer archivos */
//     if (err) {
//         console.log(err);
//     }
//     console.log(data.toString());       /* convertir los caracteres a string */
// })


//SERVIDORES
//HTTP

// const http = require('http');
const colors = require('colors');

// const handleServer = function(req, res){
//     res.writeHead(200, { 'Content-Type': 'text/html'});
//     res.write('<h1>Hello world desde node.js, hi</h1>');
//     res.end();
// }
// const server = http.createServer(handleServer);

// server.listen(3000, function () {
//     console.log('server on port 3000'.yellow)
// });



const express = require('express');

const server = express();

server.get('/', function (req, res){
    res.send('<h1>hello world express</h1>');
    res.end();
});

server.listen(3000, function(){
    console.log("server on port 3000".red);
});
